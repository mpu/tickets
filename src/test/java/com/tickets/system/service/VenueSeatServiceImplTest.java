package com.tickets.system.service;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tickets.system.SeatLocation;
import com.tickets.system.SeatStatus;
import com.tickets.system.User;
import com.tickets.system.Venue;
import com.tickets.system.exceptions.SeatExistException;
import com.tickets.system.exceptions.SeatNotAvailableException;

public class VenueSeatServiceImplTest {
	private VenueSeatService venueSeatService;
	private static final Logger log = LoggerFactory.getLogger(VenueSeatServiceImplTest.class);
	private User user;
	
	@Before
	public void setUp() throws Exception {
		venueSeatService = new VenueSeatServiceImpl();
		user = new User("007");
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testAvailableSeat() {
		Venue testVenue = new Venue("test",10,5);
		Assert.assertEquals(50,venueSeatService.checkAvailableSeats(testVenue));
		try {
			venueSeatService.reserveSeat(testVenue,new SeatLocation(10,5),user.getUserId());
			Assert.assertEquals(49,venueSeatService.checkAvailableSeats(testVenue));
		} catch (SeatExistException e) {
			e.printStackTrace();
			Assert.fail();
		}
	}

	@Test(expected=SeatExistException.class)
	public void testAvailableSeatException() throws SeatExistException {
		Venue testVenue = new Venue("test",10,10);
		Assert.assertEquals(100,venueSeatService.checkAvailableSeats(testVenue));
		venueSeatService.reserveSeat(testVenue,new SeatLocation(10,20),user.getUserId());
	}
	
	@Test(expected=SeatNotAvailableException.class)
	public void testHoldSeat() throws SeatExistException, SeatNotAvailableException{
		Venue testVenue = new Venue("test",20,30);
		Assert.assertEquals(600,venueSeatService.checkAvailableSeats(testVenue));
		try {
			Assert.assertTrue(venueSeatService.holdSeat(testVenue,new SeatLocation(20,20),user.getUserId()));
		} catch (SeatExistException e) {
			e.printStackTrace();
			Assert.fail();
		} catch (SeatNotAvailableException e) {
			e.printStackTrace();
			Assert.fail();
		}
		Assert.assertFalse(venueSeatService.holdSeat(testVenue,new SeatLocation(20,20),user.getUserId()));
	}
	
	@Test
	public void testReserveSeat(){
		Venue testVenue = new Venue("test",20,30);
		try {
			Assert.assertTrue(venueSeatService.reserveSeat(testVenue,new SeatLocation(10,20),user.getUserId()));
			Assert.assertEquals(SeatStatus.RESERVERD,testVenue.getVenueSeats()[9][19].getSeatSeatStatus());
		} catch (SeatExistException e) {
			Assert.fail();
		}
		
		try {
			venueSeatService.holdSeat(testVenue,new SeatLocation(10,20),user.getUserId());
		} catch (SeatExistException e) {
			Assert.fail();
		} catch (SeatNotAvailableException e) {
			log.info("Seat has been hold so can't be reserved");
		}
	}
	
	@Test
	public void testFindSeatStatus(){
		Venue testVenue = new Venue("test",20,30);

		try {
			Assert.assertTrue(venueSeatService.reserveSeat(testVenue,new SeatLocation(10,20),user.getUserId()));
			Assert.assertEquals(SeatStatus.RESERVERD,
					venueSeatService.getSeatStatus(testVenue, new SeatLocation(10,20)));
		} catch (SeatExistException e) {
			e.printStackTrace();
			Assert.fail();
		}
	}
	
	@Test
	public void testBestReserveSeatGroup(){
		Venue testVenue = new Venue("test",20,30);

		try {
			Assert.assertTrue(venueSeatService.bestReserveSeatGroup(testVenue,8,user.getUserId()));
			Assert.assertEquals(SeatStatus.RESERVERD,
					venueSeatService.getSeatStatus(testVenue, new SeatLocation(1,7)));
			Assert.assertEquals(SeatStatus.RESERVERD,
					venueSeatService.getSeatStatus(testVenue, new SeatLocation(1,8)));
			Assert.assertEquals(SeatStatus.AVAIABLE,
					venueSeatService.getSeatStatus(testVenue, new SeatLocation(1,9)));
			Assert.assertEquals(SeatStatus.AVAIABLE,
					venueSeatService.getSeatStatus(testVenue, new SeatLocation(2,7)));
		} catch (SeatExistException e) {
			e.printStackTrace();
			Assert.fail();
		}
	}
}