package com.tickets.system;

public class SeatLocation {
	private int seatRowIndex;
	private int seatColIndex;
	
	public SeatLocation(int seatRowIndex,int seatColIndex){
		this.seatRowIndex = seatRowIndex;
		this.seatColIndex = seatColIndex;
	}
	
	public int getSeatRowIndex() {
		return seatRowIndex;
	}
	public void setSeatRowIndex(int seatRowIndex) {
		this.seatRowIndex = seatRowIndex;
	}
	public int getSeatColIndex() {
		return seatColIndex;
	}
	public void setSeatColIndex(int seatColIndex) {
		this.seatColIndex = seatColIndex;
	}	
}