package com.tickets.system.exceptions;

public class SeatExistException extends Exception {
	private static final long serialVersionUID = 1L;

	public SeatExistException(){}
	
	public SeatExistException(String message){
		super(message);
	}
}