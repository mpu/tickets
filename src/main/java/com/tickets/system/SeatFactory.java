package com.tickets.system;

public class SeatFactory {
	public static Seat registerSeat(int seatRowLocation,int seatColLocation){
		return new Seat(new SeatLocation(seatRowLocation, seatColLocation));
	}
}