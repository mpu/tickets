package com.tickets.system;

public class Venue {
	private String venueName;
	private int seatCapacityRow;
	private int seatCapacityCol;
	private Seat[][] venueSeats;	//Seats will be defined as two-dim matrix

	public String getVenueName() {
		return venueName;
	}

	public void setVenueName(String venueName) {
		this.venueName = venueName;
	}

	public int getSeatCapacityRow() {
		return seatCapacityRow;
	}

	public void setSeatCapacityRow(int seatCapacityRow) {
		this.seatCapacityRow = seatCapacityRow;
	}

	public int getSeatCapacityCol() {
		return seatCapacityCol;
	}

	public void setSeatCapacityCol(int seatCapacityCol) {
		this.seatCapacityCol = seatCapacityCol;
	}

	public Venue(String venueName, int seatCapacityRow,int seatCapacityCol) {
		this.venueName = venueName;
		this.seatCapacityRow = seatCapacityRow;
		this.seatCapacityCol = seatCapacityCol;
		this.venueSeats = new Seat[seatCapacityRow][seatCapacityCol];

		for (int i = 0; i < seatCapacityRow; i++) {
			for(int j = 0; j < seatCapacityCol; j++){
				venueSeats[i][j] = SeatFactory.registerSeat(i,j); // Seat location start from 1
			}
		}
	}

	public Seat[][] getVenueSeats() {
		return venueSeats;
	}

	public void setVenueSeats(Seat[][] venueSeats) {
		this.venueSeats = venueSeats;
	}
	
	public int getSeatCapacity(){
		return this.getSeatCapacityCol() * this.getSeatCapacityRow();
	}
}