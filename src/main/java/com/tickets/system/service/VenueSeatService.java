package com.tickets.system.service;

import java.util.List;

import com.tickets.system.SeatLocation;
import com.tickets.system.SeatStatus;
import com.tickets.system.Venue;
import com.tickets.system.exceptions.SeatExistException;
import com.tickets.system.exceptions.SeatNotAvailableException;

public interface VenueSeatService {
	public int checkAvailableSeats(Venue venue);
	public boolean holdSeat(Venue venue,SeatLocation seatLocation,String userId) throws SeatExistException, SeatNotAvailableException;
	public boolean reserveSeat(Venue venue,SeatLocation seatLocation,String userId) throws SeatExistException;
	public boolean reserveSeatGroup(Venue venue,List<SeatLocation> seatGroup,String userId);
	public boolean bestReserveSeatGroup(Venue venue,int bestCapicity,String userId);
	public SeatStatus getSeatStatus(Venue venue,SeatLocation seatLocation) throws SeatExistException;
}