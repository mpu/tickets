package com.tickets.system.service;

import java.io.InputStream;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicInteger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.tickets.system.Seat;
import com.tickets.system.SeatLocation;
import com.tickets.system.SeatStatus;
import com.tickets.system.Venue;
import com.tickets.system.exceptions.SeatExistException;
import com.tickets.system.exceptions.SeatNotAvailableException;

public class VenueSeatServiceImpl implements VenueSeatService {
	private static final Logger log = LoggerFactory.getLogger(VenueSeatServiceImpl.class);
	
	// Check the available seats
	// If the seat status is available then it is good
	// If the seat is booked but expired then reset the seat status
	public int checkAvailableSeats(Venue venue) {
		AtomicInteger availableSeats = new AtomicInteger(0);
		Seat[][] venueSeats = venue.getVenueSeats();
		for (int i = 0; i < venueSeats.length;i++) {
			for(int j = 0;j < venueSeats[0].length;j++){
				if (venueSeats[i][j].getSeatSeatStatus() == SeatStatus.AVAIABLE) {
					availableSeats.incrementAndGet();
				} else if (venueSeats[i][j].getSeatSeatStatus() == SeatStatus.HELD) {
					if (verifyIfHoldExpire(venueSeats[i][j].getHoldTs())) {
						availableSeats.incrementAndGet();
						venueSeats[i][j].setReservedUserId("");
						venueSeats[i][j].setSeatSeatStatus(SeatStatus.AVAIABLE);
					}
				}				
			}
		}
		return availableSeats.intValue();
	}
	
	/**
	 * Hold the seat for the user
	 * 
	 * @param seatLocation
	 * @return
	 * @throws SeatExistException
	 * @throws SeatNotAvailableException
	 */
	public boolean holdSeat(Venue venue,SeatLocation seatLocation,String userId) throws SeatExistException, SeatNotAvailableException {
		if (seatLocation.getSeatRowIndex() < 0 
				|| seatLocation.getSeatRowIndex() > venue.getSeatCapacityRow()
				|| seatLocation.getSeatColIndex() < 0
				|| seatLocation.getSeatColIndex() > venue.getSeatCapacityCol())
				throw new SeatExistException("Can't find this seat as it is invalid seat location");

		Seat[][] venueSeats = venue.getVenueSeats();
		if (venueSeats[seatLocation.getSeatRowIndex()-1][seatLocation.getSeatColIndex()].getSeatSeatStatus() == SeatStatus.RESERVERD) {
			throw new SeatNotAvailableException("This seat is not available");
		} else if (venueSeats[seatLocation.getSeatRowIndex()-1][seatLocation.getSeatColIndex()].getSeatSeatStatus() == SeatStatus.HELD) {
			if (verifyIfHoldExpire(venueSeats[seatLocation.getSeatRowIndex()-1][seatLocation.getSeatColIndex()].getHoldTs())) {
				// TODO notify the another user
				venueSeats[seatLocation.getSeatRowIndex()-1][seatLocation.getSeatColIndex()].setReservedUserId(userId);
				venueSeats[seatLocation.getSeatRowIndex()-1][seatLocation.getSeatColIndex()].setSeatSeatStatus(SeatStatus.HELD); 
				venueSeats[seatLocation.getSeatRowIndex()-1][seatLocation.getSeatColIndex()].setHoldTs(new Date());
				return true;
			} else {
				throw new SeatNotAvailableException("This seat is on hold");
			}
		} else {
			venueSeats[seatLocation.getSeatRowIndex()-1][seatLocation.getSeatColIndex()].setReservedUserId(userId);
			venueSeats[seatLocation.getSeatRowIndex()-1][seatLocation.getSeatColIndex()].setSeatSeatStatus(SeatStatus.HELD);
			venueSeats[seatLocation.getSeatRowIndex()-1][seatLocation.getSeatColIndex()].setHoldTs(new Date());
			return true;
		}
	}
	
	/**
	 * 
	 * @param venue
	 * @param seatLocation
	 * @param userId
	 * @return
	 * @throws SeatExistException
	 */
	public boolean reserveSeat(Venue venue,SeatLocation seatLocation,String userId) throws SeatExistException {
		if (seatLocation.getSeatRowIndex() < 0 
			|| seatLocation.getSeatRowIndex() > venue.getSeatCapacityRow()
			|| seatLocation.getSeatColIndex() < 0
			|| seatLocation.getSeatColIndex() > venue.getSeatCapacityCol())
			throw new SeatExistException("Can't find this seat as it is invalid seat location");
		Seat[][] venueSeats = venue.getVenueSeats();

		if (venueSeats[seatLocation.getSeatRowIndex()-1][seatLocation.getSeatColIndex()-1].getSeatSeatStatus() != SeatStatus.AVAIABLE) {
			log.info("This venue {}'s seat {} is not available" ,venue.getVenueName() ,seatLocation );
			return false;
		} else {
			venueSeats[seatLocation.getSeatRowIndex()-1][seatLocation.getSeatColIndex()-1].setSeatSeatStatus(SeatStatus.RESERVERD);
			venueSeats[seatLocation.getSeatRowIndex()-1][seatLocation.getSeatColIndex()-1].setReservedUserId(userId);
			log.info("Seat {} reserverd successfully for user {} in venue {}",seatLocation,userId,venue.getVenueName());
			return true;
		}
	}

	/**
	 * TODO: currently this is not locked well for concurrency!!!
	 * @param venue
	 * @return
	 */
	public boolean reserveSeatGroup(Venue venue,List<SeatLocation> seatGroup,String userId) {
		Seat[][] venueSeats = venue.getVenueSeats();

		//First check the availability
		for (SeatLocation seatLocation : seatGroup) {
			if (venueSeats[seatLocation.getSeatRowIndex()-1][seatLocation.getSeatColIndex()].getSeatSeatStatus() != SeatStatus.AVAIABLE) {
				log.info("This venue {}'s seat {} is not available" ,venue.getVenueName() ,seatLocation );
				return false;
			}
		}
		//Then reserve
		for (SeatLocation seatLocation : seatGroup) {
			venueSeats[seatLocation.getSeatRowIndex()-1][seatLocation.getSeatColIndex()].setSeatSeatStatus(SeatStatus.RESERVERD);
			venueSeats[seatLocation.getSeatRowIndex()-1][seatLocation.getSeatColIndex()].setReservedUserId(userId);

			log.info("Seat {} reserverd successfully for user {} in venue {}",seatLocation,userId,venue.getVenueName());
		}
		return true;
	}
	
	/**
	 * 
	 * @param holdTs
	 * @return
	 */
	private boolean verifyIfHoldExpire(Date holdTs){
		try {
			Properties prop = new Properties();
			
			String propFileName = "ticketsystem.properties";
			InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(propFileName);
			
			if(inputStream != null){
				prop.load(inputStream);
			}

			Integer expireTs = Integer.valueOf(prop.getProperty("hold.expire.out"));			
			return (System.currentTimeMillis()-holdTs.getTime())>expireTs;
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return false;
	}

	public SeatStatus getSeatStatus(Venue venue, SeatLocation seatLocation) throws SeatExistException {
		if (seatLocation.getSeatRowIndex() < 0 
				|| seatLocation.getSeatRowIndex() > venue.getSeatCapacityRow()
				|| seatLocation.getSeatColIndex() < 0
				|| seatLocation.getSeatColIndex() > venue.getSeatCapacityCol())
				throw new SeatExistException("Can't find this seat as it is invalid seat location");
			
		return venue.getVenueSeats()[seatLocation.getSeatRowIndex()-1][seatLocation.getSeatColIndex()-1].getSeatSeatStatus();
	}

	/**
	 * The best way to find the best seat capacity is to return all available seats 
	 * Find a way to find as much seat for the user based on the capacity
	 */
	public boolean bestReserveSeatGroup(Venue venue, int bestCapicity, String userId) {
		log.debug("Start to find best capicity for user {}",userId);
		Seat[][] venueSeats = venue.getVenueSeats();
		if(venueSeats[0].length < bestCapicity){
			log.info("The venue can't provide {} seats in a row",bestCapicity);
		}
		
		for(int row = 0;row<venueSeats.length;row++){
			for(int col = 0;col< venueSeats[0].length-bestCapicity;col++){
				if(venueSeats[row][col].getSeatSeatStatus() == SeatStatus.AVAIABLE){
					if(hasEnoughSeat(venueSeats[row],col,bestCapicity)){
						for(int avaibaleSeatIndex = col; avaibaleSeatIndex <col+bestCapicity;avaibaleSeatIndex++){
							venueSeats[row][avaibaleSeatIndex].setSeatSeatStatus(SeatStatus.RESERVERD);
						}
						return true;
					}
				}
			}
		}
		
		return false;
	}
	
	private boolean hasEnoughSeat(Seat[] seats,int startIndex, int size){
		int start = 0;
		while(start < size){
			if(seats[startIndex+start].getSeatSeatStatus() == SeatStatus.AVAIABLE){
				start++;
			}else	return false;
		}
		return true;
	}
}