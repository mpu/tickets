package com.tickets.system;

public class VenueFactory {
	public static Venue registerVenue(String venueName,int rowCap,int colCap){
		return new Venue(venueName,rowCap,colCap);
	}
}