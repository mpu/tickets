package com.tickets.system;

public enum SeatStatus {
	AVAIABLE(1),
	RESERVERD(2),
	HELD(3);
	
	int status;
	
	SeatStatus(int status){
		this.status = status;
	}
	
	public int status(){
		return status;
	}
}