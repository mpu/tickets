package com.tickets.system;

import java.util.HashMap;
import java.util.Map;

public class TicketSystem {
	
	public static void main(String[] args) {
		System.out.println("Start the ticket system");
		Venue earthVenue = VenueFactory.registerVenue("earth",15,20);
		Venue moonVenue = VenueFactory.registerVenue("moon",15,15);
		Venue marsVenue = VenueFactory.registerVenue("mars",10,12);

		Map<String, Venue> registeredVenus = new HashMap<String,Venue>();
		registeredVenus.put(earthVenue.getVenueName(), earthVenue);
		registeredVenus.put(moonVenue.getVenueName(), moonVenue);
		registeredVenus.put(marsVenue.getVenueName(), marsVenue);
	}
}
