package com.tickets.system;

import java.util.Date;

public class Seat{
	private SeatLocation seatLocation;
	private SeatStatus seatSeatStatus;
	private Date holdTs;
	private String reservedUserId;
	
	public Seat() {
		this.seatSeatStatus = SeatStatus.AVAIABLE;
	}

	public Seat(SeatLocation seatLocation) {
		this.seatSeatStatus = SeatStatus.AVAIABLE;
		this.seatLocation = seatLocation;
	}
	
	public SeatLocation getSeatLocation() {
		return seatLocation;
	}

	public void setSeatLocation(SeatLocation seatLocation) {
		this.seatLocation = seatLocation;
	}

	public SeatStatus getSeatSeatStatus() {
		return seatSeatStatus;
	}
	public void setSeatSeatStatus(SeatStatus seatSeatStatus) {
		this.seatSeatStatus = seatSeatStatus;
	}

	public Date getHoldTs() {
		return holdTs;
	}

	public void setHoldTs(Date holdTs) {
		this.holdTs = holdTs;
	}

	public String getReservedUserId() {
		return reservedUserId;
	}

	public void setReservedUserId(String reservedUserId) {
		this.reservedUserId = reservedUserId;
	}
}